#!/bin/bash

set -o errexit

if [[ -z "$1" ]]; then
    echo No argument, defaulting to /Users/kyle/Konosuba for video location.
    echo Pass in the path yourself if you want to override it.
    konosuba_videos=/Users/kyle/Konosuba
else
    konosuba_videos=$1
fi

docker build . --tag konosuba-gifs-dev
echo
echo
echo "***************************************************"
echo "***************************************************"
echo "Open this in your browser:   http://localhost:8061/"
echo "    Ctrl-C to stop and nuke the dev container      "
echo "***************************************************"
echo "***************************************************"
echo
echo

docker run --rm --interactive --tty --publish 8061:8061 -v $konosuba_videos:/konosuba:ro konosuba-gifs-dev

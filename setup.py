import setuptools


setuptools.setup(
    name='konosuba-gifs',
    version='1.0.0',
    description='Webservice for making Konosuba GIFs, screenshots, and clips.',
    author='Kyle Wilcox',
    author_email='k.j.wilcox@gmail.com',
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=(
        'pyramid',
        'moviepy',
        'sentry-sdk',
    ),
)

#!/usr/bin/env python

import os
import random
import sys
import hashlib

import moviepy.editor
from moviepy.video.io.VideoFileClip import VideoFileClip


import pyramid.config
import pyramid.response
import wsgiref.simple_server


def listdir(dir):
    '''
    Hacky wrapper for listdir to work around the fucking garbage .DS_Store
    files OSX shits all over the fucking filesystem.
    '''
    return [
        item
        for item in os.listdir(dir)
        if item != '.DS_Store'
    ]


def get_episode_path(season: int, episode: int):

    episodes = get_episodes()

    if season == 1:
        return episodes['season_1'][episode]
    elif season == 2:
        return episodes['season_2'][episode]
    elif season == 3:
        return episodes['movies'][episode]
    else:
        return ValueError('Invalid season')

    # Old naming scheme, we no longer care about the exact name since
    # they are now split by season and number by sorted file name order.
    # prefix = os.environ.get('KONOSUBA_LOCATION', '/konosuba')
    # return os.path.join(prefix, f'Konosuba.s{season:02d}e{episode:02d}.mkv')


def load_video(*, filename, size):
    return moviepy.editor.VideoFileClip(filename, target_resolution=size)


def generate_gif(*, season, episode, minute, second, length, size=(270, None), fps=12):
    assert 1 <= season <= 3
    if season == 3:
        assert episode == 1
        assert minute < 90
    else:
        assert 1 <= episode <= 11
        assert 0 <= minute <= 25
    assert 0 <= second < 60
    assert 0 < length < 10

    output_filename = f'/tmp/kono.s{season}e{episode}.{minute}.{second}.{length}.gif'

    if os.path.exists(output_filename):
        return output_filename

    video_clip = load_video(
        filename=get_episode_path(season, episode),
        size=size,
    )

    video_clip.subclip(
        60 * minute + second,
        60 * minute + second + length,
    ).write_gif(output_filename, fps=fps)

    video_clip.close()
    del video_clip

    return output_filename


def generate_video(*, season, episode, minute, second, length, size=(270, None)):
    assert 1 <= season <= 3
    if season == 3:
        assert episode == 1
        assert minute < 90
    else:
        assert 1 <= episode <= 11
        assert 0 <= minute <= 25
    assert 0 <= second < 60
    assert 0 < length < 10

    output_filename = f'/tmp/kono.s{season}e{episode}.{minute}.{second}.{length}.mp4'

    if os.path.exists(output_filename):
        return output_filename

    video_clip = load_video(
        filename=get_episode_path(season, episode),
        size=size,
    )

    video_clip.subclip(
        60 * minute + second,
        60 * minute + second + length,
    ).write_videofile(output_filename)

    video_clip.close()
    del video_clip

    return output_filename


def get_param(request, name, intended_type, default_value, use_random, random_min, random_max):
    try:
        return intended_type(request.GET[name])
    except (ValueError, KeyError):
        pass  # Treat it like the didn't provide the parameter

    return intended_type(random.randrange(random_min, random_max + 1)) if use_random else default_value


def gif(request):
    use_random_parameters = bool(request.GET.get('random', False))

    filename = generate_gif(
        season=get_param(request, 'season', int, 1, use_random_parameters, 1, 2),
        episode=get_param(request, 'episode', int, 1, use_random_parameters, 1, 11),
        minute=get_param(request, 'm', float, 3.0, use_random_parameters, 0, 25),
        second=get_param(request, 's', float, 0.0, use_random_parameters, 0, 59),
        length=get_param(request, 'length', float, 4.0, use_random_parameters, 1, 10),
    )

    return pyramid.response.FileResponse(
        filename,
        request=request,
        content_type='image/gif'
    )


def video(request):
    use_random_parameters = bool(request.GET.get('random', False))

    filename = generate_video(
        season=get_param(request, 'season', int, 1, use_random_parameters, 1, 2),
        episode=get_param(request, 'episode', int, 1, use_random_parameters, 1, 11),
        minute=get_param(request, 'm', float, 3.0, use_random_parameters, 0, 25),
        second=get_param(request, 's', float, 0.0, use_random_parameters, 0, 59),
        length=get_param(request, 'length', float, 4.0, use_random_parameters, 1, 10),
    )

    return pyramid.response.FileResponse(
        filename,
        request=request,
        content_type='video/mp4'
    )


def generate_screenshot(*, season, episode, minute, second, size=(1080, None)):
    assert 1 <= season <= 3

    if season == 3:
        assert episode == 1
        assert minute < 90
    else:
        assert 1 <= episode <= 11
        assert 0 <= minute <= 25
    assert 0 <= second < 60

    output_filename = f'/tmp/kono.s{season}e{episode}.{minute}.{second}.png'

    if os.path.exists(output_filename):
        return output_filename

    video_clip = load_video(filename=get_episode_path(season, episode), size=size)
    video_clip.save_frame(output_filename, t=(60.0 * minute + second))
    video_clip.close()
    del video_clip

    return output_filename


def screenshot(request):
    use_random_parameters = bool(request.GET.get('random', False))

    filename = generate_screenshot(
        season=get_param(request, 'season', int, 1, use_random_parameters, 1, 2),
        episode=get_param(request, 'episode', int, 1, use_random_parameters, 1, 11),
        minute=get_param(request, 'm', float, 3.0, use_random_parameters, 0, 25),
        second=get_param(request, 's', float, 0.0, use_random_parameters, 0, 59),
    )

    return pyramid.response.FileResponse(
        filename,
        request=request,
        content_type='image/png'
    )


FORM_CONTENT = '''
<!DOCTYPE html>
<html>
<head><title>Konosuba GIFs</title></head>
<body><form method="get" action="/gif">
Season <input type="text" name="season"><br>
Episode <input type="text" name="episode"><br>
Minute <input type="text" name="m"><br>
Second <input type="text" name="s"><br>
Length <input type="text" name="length"><br>
<input type="submit">
</form>
<br>
<a href=/gif?random=1>Show me a random GIF</a>
</body>
</html>
'''


def root(request):
    return pyramid.response.Response(
        FORM_CONTENT,
        content_type='text/html',
    )


def out_of_range_param(exception, request):
    return pyramid.response.Response(
        'Sorry, one of those numbers was out of range. :(',
        status=400,
    )


def get_episodes():
    prefix = os.environ.get('KONOSUBA_LOCATION', '/konosuba')
    dirs = listdir(prefix)

    return {
        episode_group: {
            index: os.path.join(prefix, episode_group, filename)
            for index, filename in enumerate(
                sorted(listdir(os.path.join(prefix, episode_group))),
                1
            )
        }
        for episode_group in dirs
    }


def get_episode_duration(episode_group: str, episode_number: int):
    video_path = get_episodes()[episode_group][episode_number]

    with VideoFileClip(video_path) as video:
        return video.duration


def konosuba_episode_duration(request):
    episode_group = request.GET['episode_group']
    episode_number = int(request.GET['episode_number'])

    duration = get_episode_duration(episode_group, episode_number)

    return {"duration": duration}


def konosuba_episodes(request):
    episodes = get_episodes()

    return {
        directory: len(episode_list)
        for directory, episode_list in episodes.items()
    }


def simple_generate_screenshot(episode_path: str, timestamp: float):
    output_filename = hashlib.sha256(f"{episode_path}-{timestamp}".encode('utf-8')).hexdigest() + ".png"

    if os.path.exists(output_filename):
        print(f'read from cache {episode_path}-{timestamp} : {output_filename}')
        return output_filename

    video_clip = load_video(filename=episode_path, size=(1080, None))
    video_clip.save_frame(output_filename, t=timestamp)
    video_clip.close()
    del video_clip

    return output_filename


def konosuba_screenshot(request):
    episode_group = request.GET['episode_group']
    episode_number = int(request.GET['episode_number'])
    timestamp = float(request.GET['timestamp'])

    episodes = get_episodes()
    episode_path = episodes[episode_group][episode_number]

    filename = simple_generate_screenshot(episode_path, timestamp)

    return pyramid.response.FileResponse(
        filename,
        request=request,
        content_type='image/png'
    )


def create_app():
    with pyramid.config.Configurator() as config:
        config.add_route('gif', '/gif')
        config.add_route('screenshot', '/screenshot')
        config.add_route('video', '/video')

        config.add_route('konosuba-episodes', '/konosuba-episodes')
        config.add_route('konosuba-episode-duration', '/konosuba-episode-duration')
        config.add_route('konosuba-screenshot', '/konosuba-screenshot')

        config.add_route('root', '/')

        config.add_view(gif, route_name='gif')
        config.add_view(screenshot, route_name='screenshot')
        config.add_view(video, route_name='video')

        config.add_view(konosuba_episodes, route_name='konosuba-episodes', renderer='json')
        config.add_view(konosuba_episode_duration, route_name='konosuba-episode-duration', renderer='json')
        config.add_view(konosuba_screenshot, route_name='konosuba-screenshot')

        config.add_view(root, route_name='root')

        config.add_exception_view(out_of_range_param, context=AssertionError)

        return config.make_wsgi_app()


if __name__ == '__main__':
    try:
        port = int(sys.argv[-1])
    except (TypeError, ValueError):
        port = 8061

    server = wsgiref.simple_server.make_server('0.0.0.0', port, create_app())
    server.serve_forever()

import os

import konosuba_gifs.server


SENTRY_DSN = os.environ.get("SENTRY_DSN")

if SENTRY_DSN:
    import sentry_sdk
    import sentry_sdk.integrations.pyramid

    sentry_sdk.init(
        dsn=SENTRY_DSN,
        integrations=[sentry_sdk.integrations.pyramid.PyramidIntegration()],
    )

application = konosuba_gifs.server.create_app()

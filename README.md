# KonoSuba GIFs

[![pipeline status](https://gitlab.com/Kwilco/konosuba-gifs/badges/master/pipeline.svg)](https://gitlab.com/Kwilco/konosuba-gifs/commits/master)

A web service that makes GIFs and screenshots from Konosuba on demand.

This uses `moviepy` and `ffmpeg` for video processing.

![pretty flower](./pretty_flower.gif)


## Setup


### Purchase Konosuba and rip it to MKVs

Name them like `Konosuba.s01e01.mkv`. The OVAs will be considered episode 11
for each season.


### Install Docker

On Linux you can do: `sudo apt install docker.io`.

On other OS you can probably figure it out.


### Run the docker image approximately like so:

Run it:
```bash
docker run \
  --detach \
  --interactive \
  --tty \
  --name konosuba-gifs \
  --publish 8061:8061 \
  --volume /usr/share/konosuba:/konosuba:ro \
  registry.gitlab.com/kwilco/konosuba-gifs:latest
```

Other things to do:
```bash
docker logs -f konosuba-gifs
docker stop konosuba-gifs
docker start konosuba-gifs
```


### Hit the API

Available endpoints:

* `/gif`: animated GIFS up to 10 seconds long
* `/screenshot`: 720p screenshots

Available parameters:

* `season`
* `episode`
* `m` (minute), supports floats
* `s` (second), supports floats
* `length` (gif only), supports floats
* `random`

All of these parameters have defaults if you are feeling lazy.



### Development

There's a script called `localtest.sh` in the root of the repo that makes it
easy to test local changes. Run it, and it will build and run your changes in
a container, and dispose of the container afterwards. It takes one argument:
the path to your Konosuba episodes.

Example:
```bash
./localtest.sh ~/media/Konosuba
```


#### Examples

* https://example.org/gif?season=1&episode=4&m=20&s=22.3&length=4.1
* https://example.org/screenshot?episode=8&m=16

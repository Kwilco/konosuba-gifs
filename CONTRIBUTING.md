Merge Requests welcome for any unclaimed issues: 
https://gitlab.com/Kwilco/konosuba-gifs/issues

Feel free to request Developer access to the repo if you'd rather not fork to
make a Merge Request.

All code in a Merge Requests is assumed to be licensed under the GPLv3.
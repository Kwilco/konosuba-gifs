FROM python:latest

RUN apt-get update && apt-get install -yq ffmpeg

# Webserver
RUN pip install uwsgi

# Application dependencies
RUN pip install moviepy pyramid sentry-sdk

WORKDIR /app
COPY . /app
RUN pip install .

EXPOSE 8061
CMD [ "uwsgi", "--http", ":8061", "--wsgi-file", "/app/konosuba_gifs/wsgi.py", "--master", "--processes", "4" ]
